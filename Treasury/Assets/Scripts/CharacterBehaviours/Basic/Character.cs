using Data;
using UnityEngine;
using UnityEngine.AI;

namespace CharacterBehaviours
{
    [RequireComponent(typeof(CharacterVision))]
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(CharacterView))]
    public class Character : MonoBehaviour, ICharacter
    {
        public CharacterSettings CharacterData;
        private NavMeshAgent thisAgent;
        protected CharacterView _characterView;
        private CharacterVision _characterVision;

        public virtual void Start()
        {
            Debug.Log("Abobus");
            thisAgent = GetComponent<NavMeshAgent>();
            
            _characterView = GetComponent<CharacterView>();
            _characterView.Setup(CharacterData.weaponData);
            
            _characterVision = GetComponent<CharacterVision>();
            _characterVision.TargetFounded += AttackTargets;
        }
        
        public void AttackTargets(Transform[] targets)
        {
            if(targets == null) return;
            
            var closestTargetDistance = Mathf.Infinity;
            Transform closestTarget = transform;
            
            foreach (var target in targets)
            {
                var distance = Vector3.Distance(transform.position, target.position);
                
                if (distance < closestTargetDistance)
                {
                    closestTargetDistance = distance;
                    closestTarget = target;
                }
            }
            
            GoToPoint(closestTarget.position);
        }

        public void GoToPoint(Vector3 point)
        {
            thisAgent.SetDestination(point);
        }

        public void TakeDamage(int damageValue)
        {
            throw new System.NotImplementedException();
        }
        
        public void Die()
        {
            
        }
    }
}