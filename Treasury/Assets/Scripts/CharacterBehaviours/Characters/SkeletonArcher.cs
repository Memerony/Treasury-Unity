using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace CharacterBehaviours
{
    [RequireComponent(typeof(CharacterVision))]
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(CharacterView))]
    public class SkeletonArcher : Character, IAI
    {
        [SerializeField] private float patrollingRateTime = 5f;
        public override void Start()
        {
            base.Start();
            StartCoroutine(GetRandomPoint());
        }

        IEnumerator GetRandomPoint()
        {
            while (true)
            {
                yield return new WaitForSeconds(patrollingRateTime);
                Patrolling();
            }
        }

        public void Patrolling()
        {
            
        }
    }
}